//
//  ViewController.swift
//  BXMAP
//
//  Created by Jiten Bhakhda on 2/17/19.
//  Copyright © 2019 BXMAP. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnFront: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        let leftBarButton = UIBarButtonItem.init(title: "BXMAP", style: .plain, target: self, action: #selector(ViewController.closeView))
        self.navigationItem.leftBarButtonItem = leftBarButton
    }
    
    @objc func closeView() {
    }
    
    override var shouldAutorotate: Bool{
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return UIInterfaceOrientation.portrait
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }
    
    @IBAction func onTapBack(_ sender: Any) {
        self.showFullScreen(isForFront: false)
    }
    
    @IBAction func onTapFront(_ sender: Any) {
        self.showFullScreen(isForFront: true)
    }
    
    func showFullScreen(isForFront: Bool) {
        let fullscreenVC = self.storyboard?.instantiateViewController(withIdentifier: "FullScreenViewController") as! FullScreenViewController
        fullscreenVC.isForFront = isForFront
        self.navigationController?.pushViewController(fullscreenVC, animated: true)
    }
}
