//
//  FullScreenViewController.swift
//  BXMAP
//
//  Created by Jiten Bhakhda on 2/17/19.
//  Copyright © 2019 BXMAP. All rights reserved.
//

import UIKit

class FullScreenViewController: UIViewController {
    
    var currentIndex = 1
    var maxCount = 40
    
    @IBOutlet weak var imgFullScreen: UIImageView!
    var images = [String]()
    var isForFront = false
    override func viewDidLoad() {
        super.viewDidLoad()
                
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")

        let leftBarButton = UIBarButtonItem.init(title: "Close", style: .done, target: self, action: #selector(FullScreenViewController.closeView))
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        self.functionLoadImagesfor(isExitA: isForFront)
        self.imgFullScreen.image = UIImage(named: self.images[0])
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @objc func closeView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var shouldAutorotate: Bool{
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscape
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return UIInterfaceOrientation.landscapeLeft
    }
    
    func moveNext() {
        if currentIndex < (maxCount - 1) {
            currentIndex += 1
            self.imgFullScreen.image = UIImage(named: self.images[currentIndex])
        }
    }
    
    func movePrevious() {
        if currentIndex > 0 {
            currentIndex -= 1
            self.imgFullScreen.image = UIImage(named: self.images[currentIndex])
        }
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("right")
                self.movePrevious()
                
            case UISwipeGestureRecognizer.Direction.down:
                print("down")

            case UISwipeGestureRecognizer.Direction.left:
                self.moveNext()
            case UISwipeGestureRecognizer.Direction.up:
                print("up")

            default:
                break
            }
        }
    }
    
    func functionLoadImagesfor(isExitA:Bool) {
        if !isExitA {
            maxCount = 41
            for index in 1...41 {
                images.append("back_\(index).JPG")
            }
            
        }else {
            maxCount = 40
            for index in 1...40 {
                images.append("front_\(index).JPG")
            }
        }
    }
}
